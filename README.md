# remindbot

Remindbot is a **very simple** matrix bot for leightweight reminders.
It's in its early days, expect bugs, feel free to test and send feedback!

## Usage

The bot is bound to a matrix user, for my usage it is `@remindbot:myhomeserver.de`.
In a direct chat a (kind of) shell like interface is exposed:

* `notify TIMESPEC [MESSAGE]`: Send the specified message at `TIMESPEC`.
* `table`: Show the queued reminders
* `rm ID [ID [ID]…]`: Delete task(s) with `ID`.
* `help`: Show a help page

## Timespec

The timespec is simple, these variants exist:

* `5`: 5 seconds
* `5[smhd]`: 5 *s*econds, *m*inutes, *h*ours, *d*ays
* `HH:MM`: time on current day
* `tomorrow HH:MM`: time on the next day
* `YYYY:mm:dd HH:MM`: arbitrary day+time

24h format must be used.

## Run

The bot can be run on any computer from the command line.
No special setup (e.g. reverse proxy, …) is required.

## Caveats
### Only use a direct chat (for now)

Do not run the bot in a room with more people.
I want that the bot react only on mentions, but this is not implemented yet!
Currently, it parses every message which would spam the chatroom.
Only use the bot in direct conversations, for now.

### No state

Currently, the bot looses all queued tasks on restart.
This is on the radar.

### Table is global

The scope of the task table must be filtered per room.
Currently, it returns the whole table.
Do not report this as a privacy issue, please.
This will be addressed soon.

## Bugs/Patches

I assume there are lots of bugs in this fresh piece of software.
Just write an email, or send patches to: [~rumpelsepp/public-inbox@lists.sr.ht](https://lists.sr.ht/~rumpelsepp/public-inbox)
